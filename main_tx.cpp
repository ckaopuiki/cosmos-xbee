#include <iostream>
#include "device/serial/serialclass.h"

using namespace std;

int main()
{
    cout << "Hello World!" << endl;
    //    open serial port

    //  cu.usbserial-A800fcAa
    //  /dev/tty.USA19H141P1.1
    //  /dev/ttyUSB0
    Serial xbee = Serial("/dev/ttyUSB0",9600);
    xbee.set_flowcontrol(1,0);

    cout << xbee.get_error() << endl;

//    uint8_t testByte = 'A';

//    xbee.SendByte(testByte);
//    xbee.put_char(testByte);
    xbee.put_string("12345678901234567890");

    cout << xbee.get_error() << endl;



    return 0;


}
