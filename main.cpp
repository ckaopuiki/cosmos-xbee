#include <iostream>
#include "device/serial/serialclass.h"

using namespace std;

int main()
{
    cout << "Hello World!" << endl;
    //open serial port

    Serial xbee = Serial("/dev/tty.usbserial-A800fcAa",115200);
    xbee.set_flowcontrol(1,0);

    cout << xbee.get_error() << endl;

    uint8_t testByte = 'A';

    //xbee.SendByte(testByte);
    xbee.put_char(testByte);
    //xbee.put_string("xxasdsadssadassasadasd");

    cout << xbee.get_error() << endl;


    return 0;
}
