#include <iostream>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <termios.h>
#include <unistd.h>

#include "device/serial/serialclass.h"

using namespace std;

int main()
{

    cout << "Hello World!" << endl;
    //open serial port

    // /dev/tty.USA19H142P1.1
    // cu.usbserial-A10138Z1
    // /dev/ttyUSB1
    Serial xbee = Serial("/dev/ttyUSB1",115200);
    //xbee.set_flowcontrol(1,0);

    while(1){
        char test = xbee.get_char();
        cout << test << endl;
        COSMOS_SLEEP(0.5);
    }

    cout << xbee.get_error() << endl;


    return 0;
}
